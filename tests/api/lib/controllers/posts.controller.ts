import { ApiRequest } from "..//request";

export class PostsController {
  async getAllPosts() {
    const response = await new ApiRequest()
       .prefixUrl("http://tasque.lol/")
       .method("GET")
       .url('api/Posts')
       .send();
    return response;
  }
}